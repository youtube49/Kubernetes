# Kubernetes

## jsonpath to get ips of all nodes


``` kubectl
kubectl get nodes -o wide
```
``` kubectl
kubectl get nodes -o json
```

``` kubectl
kubectl get nodes -o jsonpath='{.items[*].status.addresses[?(@.type == "InternalIP")].address}' 
```

### jsonpath
- ?()	filter
- \*	wildcard. Get all objects
- @	the current object  

ref : https://kubernetes.io/docs/reference/kubectl/jsonpath/
